import abc

import torch
import pyro

from . import constants, util


def _forecast_on(obj, Nt):
    """Fast-forwards a Block-like object from sample to forecast mode.

    This does two things:

    1. `t0 -> t1`
    2. `t1 -> t1 + Nt`

    *Args:*

    + `obj (Block)`: the block to forecast
    + `Nt (int)`: the number of timesteps to forecast
    """
    setattr(obj, constants.t0, obj.t1 + 1)
    setattr(obj, constants.t1, obj.t1 + Nt + 1)


def _forecast_off(
    obj,
    t0,
    t1,
):
    """Reverses a Block-like object from forecast to sample mode.

    This does two things:

    1. `t1 -> t0`
    2. `t0 -> old t0`

    *Args:*

    + `obj (Block)`: the block to forecast
    + `t0 (int)`: the original initial time
    + `t1 (int)`: the original final time
    """

    setattr(obj, constants.t0, t0)
    setattr(obj, constants.t1, t1)


class Effect(abc.ABC):
    """
    A context manager that changes the interpretation of an STS call.

    All `Effect`s are callable. This enables the following pattern:

    ```
    my_model = sts.RandomWalk(...)  # some complex sts model
    my_effect = EffectSubclass(...)
    my_reinterpreted_model = my_effect(my_model)  # this now has the effect interpretation

    # these will now somehow be different from calling my_model(...)
    # depending on the semantics of EffectSubclass
    my_reinterpreted_results = my_reinterpreted_model(...)
    ```
    """

    def __init__(self, *args, **kwargs):
        ...

    def __call__(self, fn):
        def wrapper(*args, **kwargs):
            self.__enter__()
            result = fn(*args, **kwargs)
            self.__exit__(None, None, None)  # NOTE: useless traceback etc.
            return result
        return wrapper


class ForecastEffect(Effect):
    """Effect handler for forecasting tasks.

    From start to finish, the forecast operation consists of

    + turning off caching
    + fast-forwarding time
    + (possibly) intervening on all free parameters
    + calling sample
    + (possibly) reverting free parameter values
    + reversing time
    + returning to original cache status

    *Args:*

    + `root (block)`: the root of the STS graph
    + `Nt (int)`: number of timesteps to forecast
    + `design_tensors (Dict[str, torch.Tensor])`:
    """

    def __init__(self, root, Nt=1, design_tensors=dict()):
        self.root = root
        self.Nt = Nt
        self.design_tensors = design_tensors

        self.nodes = util.get_nodes_from_root(self.root)
        self.t0s = list()
        self.t1s = list()
        self.fms = list()
        self.datas = dict()
        self.Xs = dict()
        self.cache_mode = util.get_cache_mode(self.root)

    def __enter__(
        self,
    ):
        for node in self.nodes:
            self.t0s.append(node.t0)
            self.t1s.append(node.t1)
            self.fms.append(node._has_fast_mode)

            # handle observations
            if hasattr(node, constants.data):
                self.datas[node.name] = node.data
                node.data = None
            if hasattr(node, constants.X):
                self.Xs[node.name] = node.X
                node.X = self.design_tensors[node.name]

            _forecast_on(node, self.Nt)
            node._has_fast_mode = False
        util.set_cache_mode(
            self.root, False
        )

    def __exit__(self, type, value, traceback):
        for node, t0, t1, fm in zip(self.nodes, self.t0s, self.t1s, self.fms):
            _forecast_off(
                node,
                t0,
                t1,
            )
            node._has_fast_mode = fm

            # handle observations
            if hasattr(node, constants.data):
                node.data = self.datas[node.name]
            if hasattr(node, constants.X):
                node.X = self.Xs[node.name]

        util.set_cache_mode(self.root, self.cache_mode)
