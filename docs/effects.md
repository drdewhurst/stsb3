# `stsb3.effects`

## `Effect`
A context manager that changes the interpretation of an STS call.



## `ForecastEffect`
Effect handler for forecasting tasks.

From start to finish, the forecast operation consists of

+ turning off caching
+ fast-forwarding time
+ (possibly) intervening on all free parameters
+ calling sample
+ (possibly) reverting free parameter values
+ reversing time
+ resuming caching

*Args:*

+ `root (block)`: the root of the STS graph
+ `Nt (int)`: number of timesteps to forecast
+ `design_tensors (Dict[str, torch.Tensor])`:



## `_effect_call`
Turns an effect handler defined as a context manager into a callable.

*Args:*

`obj (Effect)`: an effect

`fn (callable)`: a callable


## `_forecast_off`
Reverses a Block-like object from forecast to sample mode.

This does two things:

1. `t1 -> t0`
2. `t0 -> old t0`

*Args:*

+ `obj (Block)`: the block to forecast
+ `t0 (int)`: the original initial time
+ `t1 (int)`: the original final time


## `_forecast_on`
Fast-forwards a Block-like object from sample to forecast mode.

This does two things:

1. `t0 -> t1`
2. `t1 -> t1 + Nt`

*Args:*

+ `obj (Block)`: the block to forecast
+ `Nt (int)`: the number of timesteps to forecast
