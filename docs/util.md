# `stsb3.util`

## `clear_cache`
Clears cache of all predecessor nodes of root.
This does *not* reset the cache mode of any node;
to turn off caching, call `set_cache_mode(root, False)`

*Args:*

+ `root (Block)`: a block
+ `memoize (bool)`: potential speedup in return for static sts graph
    assumption


## `get_graph_from_root`
Returns the compute graph with `root` as the single base node.

*Args:*

+ `root (Block)`: the root of the STS graph

*Returns:*

+ `graph (dict[list[Block...]])`: {node, [predecessor nodes]}


## `get_name2block_from_root`
Gets a `{name: block}` dict starting from the passed root node.

*Args:*

+ `root (Block)`: the root node

*Returns:*

`graph (dict)`: a dict with structure `{name : block}`


## `get_nodes_from_root`
Returns the root and all its predecessors in the graph.

Defines a BFS order on the compute graph of blocks.

*Args:*

+ `root (Block)`: the root of the STS graph
+ `memoize (bool)`: potential speedup in return for static sts graph
    assumption

*Returns:*

+ `nodes (tuple[list])`: root and predecessor nodes in the graph


## `set_cache_mode`
```
def set_cache_mode(root, cache, memoize=True):
```

Sets root and all predecessor nodes cache mode to `cache`.

*Args:*

+ `root (Block)`: a block
+ `cache (bool)`: whether or not to cache block calls
+ `memoize (bool)`: potential speedup in return for static sts graph
    assumption
