import pytest
import torch

from stsb3 import core, sts, constants, util, effects


def test_get_nodes_from_root():
    gt = sts.GlobalTrend(name="gt")
    rw = sts.RandomWalk(scale=gt.softplus(), name="rw")
    sde = sts.CCSDE(loc=gt, name="sde")
    gt2 = sts.GlobalTrend(alpha=sde, beta=rw, name="gt2")
    nodes = util.get_nodes_from_root(gt2)
    for node in [gt, rw, sde, gt2]:
        assert node in nodes


def test_get_graph_from_root():
    gt = sts.GlobalTrend(name="gt")
    rw = sts.RandomWalk(scale=gt.softplus(), name="rw")
    sde = sts.CCSDE(loc=gt, name="sde")
    gt2 = sts.GlobalTrend(alpha=sde, beta=rw, name="gt2")
    graph = util.get_graph_from_root(gt2)
    assert "sde" in graph["gt2"] and "rw" in graph["gt2"]
    assert "gt" not in graph["gt2"]
    assert graph["rw"] == graph["sde"]


def test_get_name2block_fromo_root():
    gt = sts.GlobalTrend(name="gt")
    sde = sts.CCSDE(loc=gt, name="sde")

    name2block = util.get_name2block_from_root(sde)
    assert name2block["sde"] == sde
    assert name2block["gt"] == gt
